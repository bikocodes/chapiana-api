"""Model Constants."""

PHONE_CHOICES = (
    ('M', 'Mobile'),
    ('W', 'Work'),
    ('H', 'Home'),
    ('O', 'Office')
)

COMMON_CHOICES = (
    ('W', 'Work'),
    ('H', 'Home')
)

DATE_CHOIECS = (
    ('Birthday', 'Birthday'),
    ('Anniversary', 'Anniversary')
)

RELATIONS = (
    ('Assistant', 'Assistant'),
    ('Brother', 'Brother'),
    ('Son', 'Son'),
    ('Daughter', 'Daughter'),
    ('Father', 'Father'),
    ('Mother', 'Mother'),
    ('Friend', 'Friend'),
    ('Manager', 'Manager'),
    ('Partner', 'Partner'),
    ('Sister', 'Sister'),
    ('Spouse', 'Spouse'),
    ('Relative', 'Relative')
)
